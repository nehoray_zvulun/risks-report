const Prince = require("prince");
const util = require("util");
const fs = require("fs");
const Handlebars = require("Handlebars");
const TEMPALTE_PATH = "./template.html";
const moment = require("moment");

Handlebars.registerHelper("objectLength", function (object) {
  return Object.keys(object).length;
});

Handlebars.registerHelper("getField", function (element, field) {
  return element[0][field];
});

Handlebars.registerHelper("getAffectedAssets", function (object) {
  let result = "";
  for (asset in object) {
    if (result == "") {
      result += asset;
    } else {
      result += " ," + asset;
    }
  }
  return result;
});

// read the template
let templateSorce = undefined;
try {
  templateSorce = fs.readFileSync(TEMPALTE_PATH, "utf8");
} catch (err) {
  console.error(err);
}

// compile the template
const template = Handlebars.compile(templateSorce);

// TODO: get filters, tenentId and generator
let data = {
  risks: [
    {
      affectedAssets: {},
      assetType: "EC2 Instance",
      description:
        "Letting FTP (File Transfer Protocol) exit your network is risky as it is not encrypted. It is only authenticated by simple passwords, that are transmitted in the clear. Serious vulnerabilities have been found in many versions of FTP server software. You may have many FTP servers on your internal networks and it is difficult to ensure that they are all properly hardened. A compromised or infected machine could access or damage the data on these servers. \n    ",
      status: "active",
      comment: null,
      remediationRecommendation:
        "Eliminate rules which allow access to this port to Private IPs. For file upload- use secure alternatives such as SFTP\n",
      riskTriggers: [],
      severity: "critical",
      id: "O05-NI-SG",
      name: "FTP can exit your network to Private IPs",
      vendorName: "AWS",
      affectedSubnets: {},
    },
    {
      affectedAssets: {},
      assetType: "EC2 Instance",
      description:
        " Allowing outbound SMTP access on TCP port  25/465/587 from many internal machines is risky.  Outbound SMTP (E-mail) should only originate from your public mail servers.  E-mail is a vector for many viruses and worms.  Computers that can send E-mail directly (not through your organization's designated mail servers) can bypass your network's E-mail filters, and spread viruses and worms to other networks.  Therefore, outbound SMTP access should be limited to your properly hardened public mail servers. \n    \n",
      status: "active",
      comment: null,
      remediationRecommendation:
        "Restrict the rules to refer to only the destination IPs you really use, and limit the number of public IP addresses that can be reached on this port. ",
      riskTriggers: [],
      severity: "critical",
      id: "O06-NI-SG",
      name: "SMTP can exit your network to more than 256 Private IPs",
      vendorName: "AWS",
      affectedSubnets: {},
    },
    {
      affectedAssets: {},
      assetType: "EC2 Instance",
      description:
        'Allowing the "Any" service to enter your network is extremely risky since the "Any" service includes many vulnerable services. This is risky even if the traffic is only allowed from business partners or through VPNs.',
      status: "active",
      comment: null,
      remediationRecommendation:
        'Review all the rules that allow inbound traffic with "Any" service, and limit them to those services you actually require.  ',
      riskTriggers: [],
      severity: "critical",
      id: "I01-NI-SG",
      name: '"Any" service can enter your network from Private IPs',
      vendorName: "AWS",
      affectedSubnets: {},
    },
    {
      affectedAssets: {},
      assetType: "EC2 Instance",
      description:
        "Allowing TCP on all ports to enter your network is extremely riskysince this includes many vulnerable services. This is risky even if the traffic is only allowed from business partners or through VPNs: a remote laptop connecting through a VPN could easily infect your network with a worm or virus.",
      status: "active",
      comment: null,
      remediationRecommendation:
        "Review all the rules that allow inbound traffic with TCP on all ports, and limit them to those services you actually require.  ",
      riskTriggers: [],
      severity: "critical",
      id: "I02-NI-SG",
      name: "TCP on all ports can enter your network from Private IPs",
      vendorName: "AWS",
      affectedSubnets: {},
    },
    {
      affectedAssets: {
        "AwsInstance:i-0c756fb9fgfb576e341e": {
          type: "EC2 Instance",
          accountName: "awsnew101",
          cfid: "AwsInstance:i-0c756fb9b576e341e",
          id: "i-0c756fb9b576e341e",
          name: "Ireland_WebServerSecondary_VPC_Default",
          privateIPs: ["172.31.21.142"],
          publicIPs: [],
          region: "eu-west-1",
          vpcId: "AwsVPC:vpc-168c8a70",
        },
        "AwsInstance:i-04b86cee6fgf7c8d32a3": {
          type: "EC2 Instance",
          accountName: "awsnew101",
          cfid: "AwsInstance:i-04b86cee67c8d32a3",
          id: "i-04b86cee67c8d32a3",
          privateIPs: ["172.31.29.43"],
          publicIPs: [],
          region: "eu-west-1",
          vpcId: "AwsVPC:vpc-168c8a70",
        },
        "AwsInstance:i-0c756fbfgf9b576e341e": {
          type: "EC2 Instance",
          accountName: "awsnew101",
          cfid: "AwsInstance:i-0c756fb9b576e341e",
          id: "i-0c756fb9b576e341e",
          name: "Ireland_WebServerSecondary_VPC_Default",
          privateIPs: ["172.31.21.142"],
          publicIPs: [],
          region: "eu-west-1",
          vpcId: "AwsVPC:vpc-168c8a70",
        },
        "AwsInstance:i-04bfgfg86cee67c8d32a3": {
          type: "EC2 Instance",
          accountName: "awsnew101",
          cfid: "AwsInstance:i-04b86cee67c8d32a3",
          id: "i-04b86cee67c8d32a3",
          privateIPs: ["172.31.29.43"],
          publicIPs: [],
          region: "eu-west-1",
          vpcId: "AwsVPC:vpc-168c8a70",
        },
      },
      assetType: "EC2 Instance",
      description:
        "Allowing UDP on all ports to enter your network is extremely risky since this includes many vulnerable services. This is risky even if the traffic is only allowed from business partners or through VPNs: a remote laptop connecting through a VPN could easily infect your network with a worm or virus. \n    ",
      status: "active",
      comment: null,
      remediationRecommendation:
        "Review all the rules that allow inbound traffic with UDP on all ports, and limit them to those services you \nactually require. ",
      riskTriggers: [
        {
          affectedAssetsCfids: [],
          status: {
            value: "",
            date: null,
            applyToAll: false,
            comment: null,
          },
          evidence: [
            {
              accountCfid: "AwsAccount:awsnew101",
              accountName: "",
              direction: "Inbound",
              region: "us-east-2",
              ruleCfid:
                "AwsSgIngressRule:-108545868325093364171080187921569519487",
              sgCfid: "AwsSG:sg-03c9d426313a3560c",
              sgName: "AutoSecurityGroup1",
              type: "AWSSgRule",
              vpcId: "vpc-06dcdd05be4d1946c",
              fullName:
                ":us-east-2:vpc-06dcdd05be4d1946c:AutoSecurityGroup1:Inbound",
              shortName: "AutoSecurityGroup1:Inbound",
              lastUsed: "FLOW_LOGS_DISABLED",
              failedMessage: null,
              lastUpdateTimestamp: 0,
            },
          ],
          id: 849736,
        },
        {
          affectedAssetsCfids: [],
          status: {
            value: "active",
            date: null,
            applyToAll: false,
            comment: null,
          },
          evidence: [
            {
              accountCfid: "AwsAccount:awsnew101",
              accountName: "",
              direction: "Inbound",
              region: "us-east-2",
              ruleCfid:
                "AwsSgIngressRule:31071440384498328865087644752017985511",
              sgCfid: "AwsSG:sg-06a486e2a36cb04dd",
              sgName: "AutoSecurityGroup1A",
              type: "AWSSgRule",
              vpcId: "vpc-06dcdd05be4d1946c",
              fullName:
                ":us-east-2:vpc-06dcdd05be4d1946c:AutoSecurityGroup1A:Inbound",
              shortName: "AutoSecurityGroup1A:Inbound",
              lastUsed: "FLOW_LOGS_DISABLED",
              failedMessage: null,
              lastUpdateTimestamp: 0,
            },
          ],
          id: 849737,
        },
      ],
      severity: "low",
      id: "I03-NI-SG",
      name: "UDP on all ports can enter your network from Private IPs",
      vendorName: "AWS",
      affectedSubnets: {},
    },
    {
      affectedAssets: {},
      assetType: "EC2 Instance",
      description:
        "Your cloud  LDAP server is accessible using the LDAP port (UDP/389, TCP/389).  LDAP (Lightweight Directory Access Protocol) is a directory service for controling access to network capable devices. Allowing access to these ports is risky.\n",
      status: "active",
      comment: null,
      riskTriggers: [
        {
          affectedAssetsCfids: [],
          status: {
            value: "",
            date: null,
            applyToAll: false,
            comment: null,
          },
          evidence: [
            {
              accountCfid: "AwsAccount:awsnew101",
              accountName: "",
              direction: "Inbound",
              region: "us-east-2",
              ruleCfid:
                "AwsSgIngressRule:-108545868325093364171080187921569519487",
              sgCfid: "AwsSG:sg-03c9d426313a3560c",
              sgName: "AutoSecurityGroup1",
              type: "AWSSgRule",
              vpcId: "vpc-06dcdd05be4d1946c",
              fullName:
                ":us-east-2:vpc-06dcdd05be4d1946c:AutoSecurityGroup1:Inbound",
              shortName: "AutoSecurityGroup1:Inbound",
              lastUsed: "FLOW_LOGS_DISABLED",
              failedMessage: null,
              lastUpdateTimestamp: 0,
            },
          ],
          id: 849736,
        },
        {
          affectedAssetsCfids: [],
          status: {
            value: "active",
            date: null,
            applyToAll: false,
            comment: null,
          },
          evidence: [
            {
              accountCfid: "AwsAccount:awsnew101",
              accountName: "",
              direction: "Inbound",
              region: "us-east-2",
              ruleCfid:
                "AwsSgIngressRule:31071440384498328865087644752017985511",
              sgCfid: "AwsSG:sg-06a486e2a36cb04dd",
              sgName: "AutoSecurityGroup1A",
              type: "AWSSgRule",
              vpcId: "vpc-06dcdd05be4d1946c",
              fullName:
                ":us-east-2:vpc-06dcdd05be4d1946c:AutoSecurityGroup1A:Inbound",
              shortName: "AutoSecurityGroup1A:Inbound",
              lastUsed: "FLOW_LOGS_DISABLED",
              failedMessage: null,
              lastUpdateTimestamp: 0,
            },
          ],
          id: 849737,
        },
      ],
      remediationRecommendation:
        "Eliminate rules which allow access to this port from private IPs.\n",
      riskTriggers: [],
      severity: "medium",
      id: "I04-NI-SG",
      name: "LDAP Port TCP/389, UDP/389 open from Private IPs",
      vendorName: "AWS",
      affectedSubnets: {},
    },
    {
      affectedAssets: {},
      assetType: "EC2 Instance",
      description:
        "Your cloud Hadoop database is accessible using administrative port TCP/9000. Hadoop is a framework for distributed data processing that allows access to the data and may act as database. Opening Hadoop port is risky",
      status: "active",
      comment: null,
      remediationRecommendation:
        "Eliminate rules which allow access to this port private IPs.",
      riskTriggers: [],
      severity: "medium",
      id: "I06-NI-SG",
      name: "Database port TCP/9000 can enter your network from Private IPs",
      vendorName: "AWS",
      affectedSubnets: {},
    },
  ],
  generator: "name name",
  tenantID: 1790556555666035,
  createdOn: moment().format("DD MMMM YYYY, hh:mm A"),
  filters: {
    cloud: ["AWS"],
    region: ["Africa (Cape Town)"],
    accounts: [
      "long_name_account (12345678)",
      "long_name_account (12345678)",
      "long_name_account (12345678)",
      "long_name_account (12345678)",
      "long_name_account (12345678)",
      "long_name_account (12345678)",
    ],
    tags: ["App: eCommerce", "Acoount 123456789"],
  },
};
const criticalRisks = data.risks.filter((risk) => risk.severity == "critical");
const highRisks = data.risks.filter((risk) => risk.severity == "high");
const mediumRisks = data.risks.filter((risk) => risk.severity == "medium");
const lowRisks = data.risks.filter((risk) => risk.severity == "low");

const summaryData = {
  risksCount: data.risks.length,
  suppressedRisksCount: data.risks.filter((risk) => risk.status != "active")
    .length,
  triggersCount: data.risks
    .map((risk) => risk.riskTriggers.length)
    .reduce(function (a, b) {
      return a + b;
    }, 0),
  suppressedTriggersCount: data.risks
    .map(
      (risk) =>
        risk.riskTriggers.filter((trigger) => trigger.status.value != "active")
          .length
    )
    .reduce(function (a, b) {
      return a + b;
    }, 0),
  affectedAssetsCount: "?",
  totalAssets: "?",
  criticalTriggers: criticalRisks
    .map((risk) => risk.riskTriggers.length)
    .reduce(function (a, b) {
      return a + b;
    }, 0),
  affectedAssetsByCritical: criticalRisks
    .map((risk) => Object.keys(risk.affectedAssets).length)
    .reduce(function (a, b) {
      return a + b;
    }, 0),
  highTriggers: highRisks
    .map((risk) => risk.riskTriggers.length)
    .reduce(function (a, b) {
      return a + b;
    }, 0),
  affectedAssetsByHigh: highRisks
    .map((risk) => Object.keys(risk.affectedAssets).length)
    .reduce(function (a, b) {
      return a + b;
    }, 0),
  mediumTriggers: mediumRisks
    .map((risk) => Object.keys(risk.riskTriggers).length)
    .reduce(function (a, b) {
      return a + b;
    }, 0),
  affectedAssetsByMedium: mediumRisks
    .map((risk) => Object.keys(risk.affectedAssets).length)
    .reduce(function (a, b) {
      return a + b;
    }, 0),
  lowTriggers: lowRisks
    .map((risk) => Object.keys(risk.riskTriggers).length)
    .reduce(function (a, b) {
      return a + b;
    }, 0),
  affectedAssetsByLow: lowRisks
    .map((risk) => Object.keys(risk.affectedAssets).length)
    .reduce(function (a, b) {
      return a + b;
    }, 0),

  criticalRisks: criticalRisks.length,
  highRisks: highRisks.length,
  mediumRisks: mediumRisks.length,
  lowRisks: lowRisks.length,
};

data.summaryData = summaryData;

const templateWIthData = template(data);

// create the html file of the report
try {
  fs.writeFileSync("report.html", templateWIthData, function (err) {
    if (err) {
      return console.log(err);
    }
  });
} catch (err) {
  console.error(err);
}

Prince()
  .inputs("report.html")
  .output("report1.pdf")
  .option("style", "app.component.css")
  .execute()
  .then(
    function () {
      console.log("OK: done");
    },
    function (error) {
      console.log("ERROR: ", util.inspect(error));
    }
  );
